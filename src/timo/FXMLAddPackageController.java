/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 * FXML Controller class
 *
 * @author Valtteri
 */
public class FXMLAddPackageController implements Initializable {
    @FXML
    private RadioButton classThreeRadioButton;
    @FXML
    private RadioButton classTwoRadioButton;
    @FXML
    private RadioButton classOneRadioButton;
    @FXML
    private ComboBox<String> itemsComboBox;
    @FXML
    private ComboBox<String> arrivalSmartPostComboBox;
    @FXML
    private ComboBox<String> arrivalTownComboBox;
    @FXML
    private ComboBox<String> departureSmartPostComboBox;
    @FXML
    private ComboBox<String> departureTownComboBox;
    @FXML
    private TextField itemWeightTextField;
    @FXML
    private TextField itemNameTextField;
    @FXML
    private Button finishPackageCreationButton;
    @FXML
    private Button cancelPackageCreationButton;
    @FXML
    private Button infoAboutClassesButton;
    @FXML
    private TextField itemSizeTextField;
    @FXML
    private CheckBox isItemFragileCheckBox;
    @FXML
    private ToggleGroup classesRadioGroup;
    //Used for showing user error messages if terms of the selected class aren't met.
    @FXML
    private Label errorLabel;
    //Used for calculating distances between selected smartposts.
    @FXML
    private WebView web;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            itemsComboBox.getItems().add("uusi esine");
            itemsComboBox.getItems().add("KaunaritDVD");
            itemsComboBox.getItems().add("Television");
            itemsComboBox.getItems().add("MyLittlePonyFigure");
            itemsComboBox.getItems().add("Book");
            DataBuilder db = DataBuilder.getInstance();
            for (String city : db.getCityList()){
                arrivalTownComboBox.getItems().add(city);
                departureTownComboBox.getItems().add(city);
            }
        } catch (IOException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        }
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
    }    
    
    //Creates a package and closes the packageCreation window.
    @FXML
    private void finishPackageCreation(ActionEvent event){
        errorLabel.setText("");
        Storage s = Storage.getInstance();
        //makes arraylist of coordinates of smartposts to calculate distances.
        ArrayList<String> geopoints = new ArrayList();
        geopoints.add(getSmartPost(departureTownComboBox.getValue(),departureSmartPostComboBox.getValue()).getGeoPoint().getLatitude()); 
        geopoints.add(getSmartPost(departureTownComboBox.getValue(),departureSmartPostComboBox.getValue()).getGeoPoint().getLongitude());
        geopoints.add(getSmartPost(arrivalTownComboBox.getValue(),arrivalSmartPostComboBox.getValue()).getGeoPoint().getLatitude());
        geopoints.add(getSmartPost(arrivalTownComboBox.getValue(),arrivalSmartPostComboBox.getValue()).getGeoPoint().getLongitude());
        //uses javascript function to calculate distance between selected smartposts.
        double distance = Double.parseDouble(web.getEngine().executeScript("document.createPath("+geopoints+",'blue', 1)").toString());
        try {         
            if(classOneRadioButton.isSelected()){
                if(isItemFragileCheckBox.isSelected()){
                    throw new PackageCreationException("1. Luokkaan ei särkyvää");
                }
                //class1's max distance is 150.
                if(distance > 150){
                    
                    throw new FirstClassException("1. Luokan maksimietäisyys 150 km", distance);
                      
                }
                else{
                    FirstClass newpackage = new FirstClass(Double.parseDouble(itemSizeTextField.getText()),
                    Double.parseDouble(itemWeightTextField.getText()),isItemFragileCheckBox.isSelected(),
                    getSmartPost(departureTownComboBox.getValue(),departureSmartPostComboBox.getValue()),
                    getSmartPost(arrivalTownComboBox.getValue(),arrivalSmartPostComboBox.getValue()), 
                    new Item(itemNameTextField.getText(),Double.parseDouble(itemWeightTextField.getText()),
                    Double.parseDouble(itemWeightTextField.getText()), isItemFragileCheckBox.isSelected()));
                
                    s.getPackageList().add(newpackage);
            
                    Stage scene = (Stage) cancelPackageCreationButton.getScene().getWindow();
                    scene.close();}
            }
            else if(classTwoRadioButton.isSelected()){
                if(isItemFragileCheckBox.isSelected() && 0.05 < Double.parseDouble(itemSizeTextField.getText())){
                    throw new PackageCreationException("2. luokassa paketin oltava alle 0.05 m^3 , jos särkyvää");
                }
                SecondClass newpackage = new SecondClass(Double.parseDouble(itemSizeTextField.getText()),
                    Double.parseDouble(itemWeightTextField.getText()),isItemFragileCheckBox.isSelected(),
                    getSmartPost(departureTownComboBox.getValue(),departureSmartPostComboBox.getValue()),
                    getSmartPost(arrivalTownComboBox.getValue(),arrivalSmartPostComboBox.getValue()), 
                    new Item(itemNameTextField.getText(),Double.parseDouble(itemWeightTextField.getText()),
                    Double.parseDouble(itemWeightTextField.getText()), isItemFragileCheckBox.isSelected()));
            
                s.getPackageList().add(newpackage);
            
                Stage scene = (Stage) cancelPackageCreationButton.getScene().getWindow();
                scene.close();
            }
            else if(classThreeRadioButton.isSelected()){
                if(isItemFragileCheckBox.isSelected()){
                    throw new PackageCreationException("3. Luokkaan ei särkyvää");
                }
                ThirdClass newpackage = new ThirdClass(Double.parseDouble(itemSizeTextField.getText()),
                    Double.parseDouble(itemWeightTextField.getText()),isItemFragileCheckBox.isSelected(),
                    getSmartPost(departureTownComboBox.getValue(),departureSmartPostComboBox.getValue()),
                    getSmartPost(arrivalTownComboBox.getValue(),arrivalSmartPostComboBox.getValue()), 
                    new Item(itemNameTextField.getText(),Double.parseDouble(itemWeightTextField.getText()),
                    Double.parseDouble(itemWeightTextField.getText()), isItemFragileCheckBox.isSelected()));
            
                s.getPackageList().add(newpackage);
            
                Stage scene = (Stage) cancelPackageCreationButton.getScene().getWindow();
                scene.close();
            }
            else {
                try {
                    throw new PackageCreationException("Valitse luokka");
                } catch (PackageCreationException ex) {
                    errorLabel.setText(ex.getMessage());
                }
            }
        }
        catch (NumberFormatException ex){
            errorLabel.setText(ex.getMessage());
        } catch (FirstClassException ex) {
            errorLabel.setText(ex.getMessage() + " Valittujen smartpostien välinen etaisyys: " + ex.getLength());
        } catch (PackageCreationException ex) {
            errorLabel.setText(ex.getMessage());
        } 
    }

    @FXML
    private void cancelPackageCreation(ActionEvent event) {
        Stage scene = (Stage) cancelPackageCreationButton.getScene().getWindow();
        scene.close();
    }
    
    //Opens window that shows user info about classes
    @FXML
    private void showInfoAboutClasses(ActionEvent event) {
        try {
            Stage addpackage = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLClassInfo.fxml"));
            Scene scene = new Scene(page);
            addpackage.setScene(scene);
            addpackage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //fills departure smartpost combobox based on departure town user has selected.
    @FXML
    private void fillDepartureSmartPostComboBox(ActionEvent event) {
        try {
            String city = departureTownComboBox.getValue();
            DataBuilder db = DataBuilder.getInstance();
            departureSmartPostComboBox.getItems().clear();
            for (SmartPost smartpost : db.getSmartPostList()){
                if(smartpost.getCity().equals(city)){
                    departureSmartPostComboBox.getItems().add(smartpost.getPostOffice());
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
    
    @FXML
    private void fillArrivalSmartPostComboBox(ActionEvent event) {
        try {
            String city = arrivalTownComboBox.getValue();
            DataBuilder db = DataBuilder.getInstance();
            arrivalSmartPostComboBox.getItems().clear();
            for (SmartPost smartpost : db.getSmartPostList()){
                if(smartpost.getCity().equals(city)){
                    arrivalSmartPostComboBox.getItems().add(smartpost.getPostOffice());
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    }
    
    //function takes city and postoffice parameters and returns smartpost 
    //from DataBuilder's smartpostlist that has same city and postoffice
    public SmartPost getSmartPost (String c, String p){
        SmartPost sp = null;
        try {
            DataBuilder db = DataBuilder.getInstance();
            
            for (SmartPost smartpost : db.getSmartPostList()){
                if(smartpost.getCity().equals(c)){
                    if(smartpost.getPostOffice().equals(p)){
                        sp = smartpost;
                    }
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sp;
    }
    
    //fills textfields and the checkbox based on itemComboBox value.
    @FXML
    private void fillProperties(ActionEvent event) {
        if(itemsComboBox.getValue().equals("uusi esine")){
            itemNameTextField.setText("");
            itemWeightTextField.setText("");
            itemSizeTextField.setText("");
            isItemFragileCheckBox.setSelected(false);
            enableInputThings();
        }
        
        else if(itemsComboBox.getValue().equals("KaunaritDVD")){
            KaunaritDVD kaunari = new KaunaritDVD();
            itemNameTextField.setText(kaunari.getName());
            itemWeightTextField.setText(Double.toString(kaunari.getWeight()));
            itemSizeTextField.setText(Double.toString(kaunari.getSize()));
            if(kaunari.getIsFragile()){
                isItemFragileCheckBox.setSelected(true);
            }
            else {
                isItemFragileCheckBox.setSelected(false);
            }
            disableInputThings();
        }
        
        else if(itemsComboBox.getValue().equals("MyLittlePonyFigure")){
            MyLittlePonyFigure figure = new MyLittlePonyFigure();
            itemNameTextField.setText(figure.getName());
            itemWeightTextField.setText(Double.toString(figure.getWeight()));
            itemSizeTextField.setText(Double.toString(figure.getSize()));
            if(figure.getIsFragile()){
                isItemFragileCheckBox.setSelected(true);
            }
            else {
                isItemFragileCheckBox.setSelected(false);
            }
            disableInputThings();
        }
        
        else if(itemsComboBox.getValue().equals("Book")){
            Book book = new Book();
            itemNameTextField.setText(book.getName());
            itemWeightTextField.setText(Double.toString(book.getWeight()));
            itemSizeTextField.setText(Double.toString(book.getSize()));
            if(book.getIsFragile()){
                isItemFragileCheckBox.setSelected(true);
            }
            else {
                isItemFragileCheckBox.setSelected(false);
            }
            disableInputThings();
        }
        
        else if(itemsComboBox.getValue().equals("Television")){
            Television tv = new Television();
            itemNameTextField.setText(tv.getName());
            itemWeightTextField.setText(Double.toString(tv.getWeight()));
            itemSizeTextField.setText(Double.toString(tv.getSize()));
            if(tv.getIsFragile()){
                isItemFragileCheckBox.setSelected(true);
            }
            else {
                isItemFragileCheckBox.setSelected(false);
            }
            disableInputThings();
        }
    }
    
    //Used when user doesn't make new item.
    public void disableInputThings(){
        itemNameTextField.setDisable(true);
        itemWeightTextField.setDisable(true);
        itemSizeTextField.setDisable(true);
        isItemFragileCheckBox.setDisable(true);
    }
    
    public void enableInputThings(){
        itemNameTextField.setDisable(false);
        itemWeightTextField.setDisable(false);
        itemSizeTextField.setDisable(false);
        isItemFragileCheckBox.setDisable(false);
    }
    
    public class PackageCreationException extends Exception{
        public PackageCreationException(String s){
            super(s);
        }
    }
    
    public class FirstClassException extends Exception{
        private double length;
        public FirstClassException(String s, double l){
            super(s);
            length = l;
        }
        
        public double getLength(){return length;}
    }
}
