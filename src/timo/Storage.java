/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Valtteri
 */
public class Storage {
    private static Storage instance = null;
    private ArrayList<Package> packagesList = new ArrayList();
    
    //Storage object has list of object that are in storage. Singleton
    private Storage(){
        try {
            DataBuilder db = DataBuilder.getInstance();
            ArrayList<SmartPost> smartpostlist = db.getSmartPostList();
        } catch (IOException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Storage getInstance(){
        if(instance == null){
            instance = new Storage();
        }
        else{
            System.out.println("Instance already exists.");
        }
        return instance;
    }
    
    public ArrayList<Package> getPackageList(){return packagesList;}
}