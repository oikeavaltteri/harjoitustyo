/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Valtteri
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private Button addToMapButton;
    @FXML
    private ComboBox<String> smartPostComboBox;
    @FXML
    private Button createPackageButton;
    @FXML
    private ComboBox<Package> createdPackagesComboBox;
    @FXML
    private Button removeRoutesButton;
    @FXML
    private Button updatePackageButton;
    @FXML
    private Button sendPackageButton;
    @FXML
    private WebView web;
    @FXML
    private Label numberOfPackagesLabel;
    @FXML
    private TextArea packagesInformationTextArea;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        try {
            DataBuilder db = DataBuilder.getInstance();
            Storage s = Storage.getInstance();
            for (String city : db.getCityList()){
                smartPostComboBox.getItems().add(city);
            }
            for (Package onepackage : s.getPackageList()){
                createdPackagesComboBox.getItems().add(onepackage);
                System.out.println(onepackage.toString());
                packagesInformationTextArea.setText(packagesInformationTextArea.getText() + onepackage+" Nimi: "+
                onepackage.getItem().getName() + " Paino: " + onepackage.getWeight() + " Koko: " + onepackage.getSize() + 
                " Lähetyspaikka: " + onepackage.getDepartureSmartPost().getPostOffice() + " ,"+onepackage.getDepartureSmartPost().getCity() +
                " Saapumispaikka: "+ onepackage.getArrivalSmartPost().getPostOffice() + " ,"+onepackage.getArrivalSmartPost().getCity() +
                "\n\n");
            }
            numberOfPackagesLabel.setText("Paketteja varastossa: "+ s.getPackageList().size());
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
    }    
    
    //adds selected town's smartposts to map
    @FXML
    private void addToMap(ActionEvent event) {
        try {
            String city = smartPostComboBox.getValue();
            DataBuilder db = DataBuilder.getInstance();
            for (SmartPost smartpost : db.getSmartPostList()){
                if(smartpost.getCity().equals(city)){
                    web.getEngine().executeScript("document.goToLocation('"+smartpost.getAddress()+
                    ", "+smartpost.getCode()+" "+smartpost.getCity()+"', '"+smartpost.getPostOffice()+
                    ", "+smartpost.getAvailability()+"', 'red')");
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(FXMLAddPackageController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void startAddPackage(ActionEvent event) {
        try {
            Stage addpackage = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLAddPackage.fxml"));
            Scene scene = new Scene(page);
            addpackage.setScene(scene);
            addpackage.show();
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void RemoveRoutes(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    private void updatePackage(ActionEvent event) {
        update();
    }
    
    //updates createdPackagesComboBox and "Paketit"-Tab that shows current storage status.
    public void update(){
        createdPackagesComboBox.getItems().clear();
        packagesInformationTextArea.setText("");
        Storage s = Storage.getInstance();
        for (Package onepackage : s.getPackageList()){
            createdPackagesComboBox.getItems().add(onepackage);
            System.out.println(onepackage.toString());
                
            packagesInformationTextArea.setText(packagesInformationTextArea.getText() + onepackage+" Nimi: "+
            onepackage.getItem().getName() + " Paino: " + onepackage.getWeight() + " Koko: " + onepackage.getSize() + 
            " Särkyvää: " + onepackage.getIsFragile() +
            " Lähetyspaikka: " + onepackage.getDepartureSmartPost().getPostOffice() + " ,"+onepackage.getDepartureSmartPost().getCity() +
            " Saapumispaikka: "+ onepackage.getArrivalSmartPost().getPostOffice() + ", "+onepackage.getArrivalSmartPost().getCity() +
            "\n\n");
        }
        numberOfPackagesLabel.setText("Paketteja varastossa: "+ s.getPackageList().size());
    }
    
    //sends the selected package and removes it from storage.
    @FXML
    private void sendPackage(ActionEvent event) {
        
        ArrayList<String> geopoints = new ArrayList();
        geopoints.add(createdPackagesComboBox.getValue().getDepartureSmartPost().getGeoPoint().getLatitude());
        geopoints.add(createdPackagesComboBox.getValue().getDepartureSmartPost().getGeoPoint().getLongitude());
        geopoints.add(createdPackagesComboBox.getValue().getArrivalSmartPost().getGeoPoint().getLatitude());
        geopoints.add(createdPackagesComboBox.getValue().getArrivalSmartPost().getGeoPoint().getLongitude());
        web.getEngine().executeScript("document.createPath("+geopoints+",'blue',"+createdPackagesComboBox.getValue().getPackageClass()+")");
        System.out.println(web.getEngine().executeScript("document.createPath("+geopoints+",'blue',"+createdPackagesComboBox.getValue().getPackageClass()+")"));
        Storage s = Storage.getInstance();
        s.getPackageList().remove(createdPackagesComboBox.getValue());
        update();
        
    }
    
}
