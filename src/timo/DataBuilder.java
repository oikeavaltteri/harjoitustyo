/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Valtteri
 */
public class DataBuilder {
    private static DataBuilder instance = null;
    private ArrayList<SmartPost> smartpostlist = new ArrayList();
    //List of towns with smartposts.
    private ArrayList<String> citylist = new ArrayList();
    
    //Creates DataBuilder object that contains information of smartposts. Gets smartpostdata from URL.
    //Singleton
    private DataBuilder(){
        try {
            Document doc;
            URL url = new URL("http://smartpost.ee/fi_apt.xml");
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            String content = "";
            String line;
            while((line = br.readLine()) != null){
                content += line + "\n";
            }
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            
            NodeList nodes = doc.getElementsByTagName("place");
            String city = "";
            for (int i = 0; i < nodes.getLength(); i++){
                Node node = nodes.item(i);
                Element e = (Element) node;
                if(!city.equals(getValue("city",e))){
                    city = getValue("city",e);
                    citylist.add(city);
                }
                SmartPost sm = new SmartPost(getValue("code",e),getValue("city",e),
                getValue("address",e),getValue("availability",e),getValue("postoffice",e),
                getValue("lat",e),getValue("lng",e));
                smartpostlist.add(sm);
                
            };
            System.out.println(citylist);
            System.out.println(smartpostlist);
        } catch (MalformedURLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private String getValue(String tag, Element e) {
        return ((Element) (e.getElementsByTagName(tag).item(0))).getTextContent();
    }
    
    public ArrayList<SmartPost> getSmartPostList(){return smartpostlist;}
    public ArrayList<String> getCityList(){return citylist;}
    
    public static DataBuilder getInstance() throws MalformedURLException, IOException, ParserConfigurationException, SAXException{
        if(instance == null){
            instance = new DataBuilder();
        }
        else{
            System.out.println("Instance already exists.");
        }
        return instance;
    }
    
    
}
