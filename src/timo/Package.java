/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author Valtteri
 */
public abstract class Package {
    double size;
    private double weight;
    private boolean isFragile;
    private SmartPost departureSmartPost;
    private SmartPost arrivalSmartPost;
    private int packageClass;
    private Item item;
    
    protected Package(double s, double w, boolean f, SmartPost d, SmartPost a, int p, Item i){
        size = s;
        weight = w;
        isFragile = f;
        departureSmartPost = d;
        arrivalSmartPost = a;
        packageClass = p;
        item = i;
    }
    
    @Override public String toString(){
        return this.getItem().getName()+", Class"+this.getPackageClass()+", "
        +this.getDepartureSmartPost()+", "+this.getArrivalSmartPost();
    }
    
    protected double getSize(){return size;}
    protected double getWeight(){return weight;}
    protected boolean getIsFragile(){return isFragile;}
    protected SmartPost getDepartureSmartPost(){return departureSmartPost;}
    protected SmartPost getArrivalSmartPost(){return arrivalSmartPost;}
    protected int getPackageClass(){return packageClass;}
    protected Item getItem(){return item;}
    
    
}
