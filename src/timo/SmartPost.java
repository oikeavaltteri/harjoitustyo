/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author Valtteri
 */
public class SmartPost {
    private String code;
    private String city;
    private String address;
    private String availability;
    private String postoffice;
    private GeoPoint geopoint;
    public SmartPost(String co, String ci, String ad, String av, String po, String lat, String lng){
        code = co;
        city = ci;
        address = ad;
        availability = av;
        postoffice = po;
        geopoint = new GeoPoint(lat, lng);
    }
    public String getCode(){return code;}
    public String getCity(){return city;}
    public String getAddress(){return address;}
    public String getAvailability(){return availability;}
    public String getPostOffice(){return postoffice;}
    public GeoPoint getGeoPoint(){return geopoint;}
    
    //makes object that has coordinates of smartpost.
    public class GeoPoint {
        private String latitude;
        private String longitude;
        public GeoPoint(String lat,String lng){
            latitude = lat;
            longitude = lng;
        }
        public String getLatitude(){return latitude;}
        public String getLongitude(){return longitude;}
    }
    
    @Override public String toString(){
        return this.getPostOffice();
    }
}
