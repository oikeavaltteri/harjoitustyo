/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author Valtteri
 */
public class Item {
    private final String name;
    private final double size;
    private final double weight;
    private final boolean isFragile;
    protected Item(String n, double s, double w, boolean f){
        name = n;
        size = s;
        weight = w;
        isFragile = f;        
    }
    protected String getName(){return name;}
    protected double getSize(){return size;}
    protected double getWeight(){return weight;}
    protected boolean getIsFragile(){return isFragile;}
    
    
    
}
